import { useState } from "react";
import { CounterItem } from "./CounterItem";
import "./App.css";

function App() {
  let countersMetadata = [
    { id: 0, value: "zero", isClicked: false },
    { id: 1, value: "zero", isClicked: false },
    { id: 2, value: "zero", isClicked: false },
    { id: 3, value: "zero", isClicked: false },
  ];

  const [counters, setCounters] = useState(countersMetadata);
  const [totalCount, setTotalCount] = useState(0);

  const incrementFunction = (id) => {
    let incrementedCounter = counters.map((counter) => {
      if (counter.id === id) {
        if (counter.value === "zero") {
          counter.value = 1;
        } else {
          counter.value += 1;
        }
      }

      return counter;
    });

    setCounters(incrementedCounter);
  };

  const decrementFunction = (id) => {
    let decrementedCounter = counters.map((counter) => {
      if (counter.id === id) {
        if (counter.value > 1) {
          counter.value -= 1;
        } else {
          counter.value = "zero";
        }
      }

      return counter;
    });

    setCounters(decrementedCounter);
  };

  const updateTotalItemsClicked = (id) => {
    let trueClicked = counters.filter((item) => {
      if (item.id === id && item.isClicked === false) {
        item.isClicked = true;
      } else if (item.id === id && item.isClicked === true) {
        item.isClicked = false;
      }

      if (item.isClicked === true) return item;
    });
    setTotalCount(trueClicked.length);
  };

  const deleteCounter = (id) => {
    let totalCount = 0;
    let modArray = counters.filter((element) => {
      if (element.id !== id) {
        if (element.isClicked === true) totalCount++;
        return element;
      }
    });

    setCounters(modArray);
    setTotalCount(totalCount);
  };

  const refreshPage = () => {
    if (counters.length === 0) {
      window.location.reload();
    }
  };

  const resetPage = () => {
    let resetCounters = counters.map((counter) => {
      counter.value = "zero";
      counter.isClicked = false;
      return counter;
    });

    setCounters(resetCounters);
    setTotalCount(0)
  };

  return (
    <>
      <div className="main-container">
        <div className="counter-container">
          <div className="cart-panel">
            <div className="cart-icon-container">
              <img className="icon" src="/cart.png" />
            </div>
            <div className="total-count-holder">
              <div className="total-count">{totalCount}</div>
            </div>
            <div className="item-text">Items</div>
          </div>

          <div className="control-panel">
            <div onClick={resetPage} className="reset-icon-container ">
              <img className="small-icon " src="/reset.png" />
            </div>
            <div onClick={refreshPage} className="refresh-icon-container">
              <img className="small-icon " src="/refresh.png" />
            </div>
          </div>

          <div className="count-operation-container">
            {counters.map((metadata) => {
              return (
                <CounterItem
                  id={metadata.id}
                  countValue={metadata.value}
                  increment={incrementFunction}
                  decrement={decrementFunction}
                  deleteCounter={deleteCounter}
                  updateTotalItem={updateTotalItemsClicked}
                />
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
