export const CounterItem = (props) => {
  let { id, countValue, increment, decrement, deleteCounter, updateTotalItem } = props;

  const incrementFunction = () => {
    increment(id);
  };

  const decrementFunction = () => {
    decrement(id);
  };

  const hideCounter = () => {
    deleteCounter(id);
  };

  const incrementTotalCount = () => {
    if (countValue === "zero") updateTotalItem(id);
  };

  const decrementTotalCount = () => {
    if (countValue === 1) updateTotalItem(id);
  };

  return (
    <>
      <div className="count-operations">
        <div className="count">{countValue}</div>
        <button
          onClick={() => {
            incrementTotalCount();
            incrementFunction();
          }}
          className="increment"
        >
          <img
            className="small-icon"
            src="/plus.png"
            alt="Add small-icon"
          ></img>
        </button>
        <button
          onClick={() => {
            decrementFunction();
            decrementTotalCount();
          }}
          className="decrement"
        >
          <img
            className="small-icon"
            src="/minus.png"
            alt="subtract small-icon"
          ></img>
        </button>
        <button onClick={hideCounter} className="delete">
          <img className="small-icon" src="/delete.png" alt="delete icon"></img>
        </button>
      </div>
    </>
  );
};
